/*
 * ----------------------------------------------------------------------------
 * "THE BEER-WARE LICENSE" (Revision 42):
 * <mlisdim@ya.ru> wrote this file.  As long as you retain this notice you
 * can do whatever you want with this stuff. If we meet some day, and you think
 * this stuff is worth it, you can buy me a beer in return.   Lisin D.A.
 * ----------------------------------------------------------------------------
 */

#include <cstdlib>
#include <cstdint>
#include <assert.h>

#include "intArray.h"

#include "debug.h"

using namespace std;

uint32_t IntArray::m_count = 0;
const uint32_t IntArray::M_MEM_BLOCK_SIZE = 1;

/************** Class methods **************/

IntArray::IntArray()
        :m_array(nullptr),
         m_size(0)
{

}

IntArray::IntArray(uint32_t size) {
	init(size);
}

IntArray::IntArray(uint32_t size, int32_t initValue) {
    init(size, initValue);
}

IntArray::IntArray(IntArray &&source) :
        m_array(source.m_array),
        m_size(source.m_size)
{
    source.m_array = nullptr;
    source.m_size = 0;
}

IntArray::IntArray(const IntArray& sourceObj) {
    fSay("Copy array " << &sourceObj << " to " << this);
    init(sourceObj.getSize());
    for (int32_t i = 0; i < m_size; ++i) {
        m_array[i] = sourceObj.m_array[i];
    }
}

IntArray::~IntArray() {
	m_count -= m_size;
	if (m_array) {
		free(m_array);
	}
}

void IntArray::init(uint32_t size, int32_t initValue) {
    assert(size > 0);

    m_size = size;

    try {
        m_allocMem();
    }
    catch (bad_alloc& e) {
        debug(e.what());
        abort();
    }

    for (int i = 0; i < size; ++i){
        m_array[i] = initValue;
    }
    m_count += size;
}

void IntArray::clone(const IntArray& sourceObj) {
    fSay("Coping " << &sourceObj << " to " << this);

    if(m_array) {
        m_count -= m_size;
        free(m_array);
    }
    init(sourceObj.getSize());
    for (int32_t i = 0; i < m_size; ++i) {
        m_array[i] = sourceObj.m_array[i];
    }
}

uint32_t IntArray::getCount() {
	return m_count;
}

int32_t& IntArray::at(int32_t i) const {
	assert(i >= 0 && i < m_size);
    assert(m_array);

	return m_array[i];
}

void IntArray::set(int32_t n, int32_t value) {
	assert(n >= 0 && n < m_size);
    assert(m_array);
	
	m_array[n] = value;
}

uint32_t IntArray::getSize() const {
	return m_size;
}

void IntArray::pushBack(int32_t value) {
    fSay("Pushing value " << value << " into " << this);

    ++m_size;

    try {
        m_tryIncMem();
    }
    catch (bad_alloc& e) {
        debug(e.what());
        abort();
    }

    m_array[m_size-1] = value;
    m_count += 1;
}

void IntArray::pushBack(const IntArray &addObj) {
    fSay("Pushing array " << &addObj << " into " << this);

    const uint32_t addObjSize = addObj.getSize();
    m_size += addObjSize;

    try {
        m_tryIncMem();
    }
    catch (bad_alloc& e) {
        debug(e.what());
        abort();
    }

    for (uint32_t i = m_size - addObjSize;
         i < m_size;
         ++i)
    {
        m_array[i] = addObj.at(i - m_size + addObjSize);
    }

    m_count += addObjSize;
}

int32_t IntArray::popBack() {
    if (m_size > 0) {
        int32_t retValue = m_array[m_size - 1];
        m_size--;

        try {
            m_tryDecMem();
        }
        catch (bad_alloc& e) {
            debug(e.what());
            abort();
        }

        return retValue;
    }
    return 0;
}

int32_t& IntArray::operator[](int32_t i) {
    assert(m_array);
    return m_array[i];
}

IntArray& IntArray::operator=(const IntArray &scrObj) {
    fSay("Coping " << &scrObj << " to " << this);
    this->clone(scrObj);

    return *this;
}

IntArray& IntArray::operator+= (const IntArray& srcObj) {
    fSay("Adding " << &srcObj << " to " << this);
    this->pushBack(srcObj);

    return *this;
}

inline void IntArray::m_tryIncMem() {
    const size_t reqMemSize = sizeof(uint32_t) * m_size;
    size_t newMemSize = m_memSize;
    uint32_t isIncNeeded = 0;

    while (newMemSize < reqMemSize) {       // Find new optimal memory size
        newMemSize *= 2;                    // by multiply on 2
        ++isIncNeeded;
    }
    if (isIncNeeded) m_resizeMem(newMemSize);
}

inline void IntArray::m_tryDecMem() {
    const size_t reqMemSize =               // Double size because we must
            sizeof(uint32_t) * m_size * 2;  // find memory strict more then required
    size_t newMemSize = m_memSize;          // but not more then reqMemSize * 2
    uint32_t isDecNeeded = 0;

    while (newMemSize > reqMemSize) {       // Find new optimal memory size
        newMemSize /= 2;                    // by division on 2
        ++isDecNeeded;
    }

    if (isDecNeeded) m_resizeMem(newMemSize);
}

void IntArray::m_resizeMem(size_t memSize) {
    assert(m_array);

    int32_t* testPrt = nullptr;
    testPrt = static_cast<int32_t*>(
            realloc(m_array, memSize)
    );
    if (!testPrt) {
        throw bad_alloc();
    }

    m_memSize = static_cast<uint32_t>(memSize);
    m_array = testPrt;
}

void IntArray::m_allocMem() {
    size_t memSize = sizeof(int32_t);
    while (m_size > memSize/sizeof(int32_t)) {
        memSize *= 2;
    }

    int32_t* testPrt = static_cast<int32_t*>(
            malloc(memSize * sizeof(int32_t))
    );
    if (!testPrt) {
        throw bad_alloc();
    }

    m_memSize = static_cast<uint32_t>(memSize);
    m_array = testPrt;
}

/************** Friend functions **************/

IntArray operator+ (const IntArray& array1, const IntArray& array2) {
    fSay("Adding " << &array1 << " to " << &array2);

    const uint32_t resArraySize =
            array1.getSize() + array2.getSize();

    IntArray resArray(resArraySize);
    for (int32_t i = 0; i < array1.getSize(); ++i) {
        resArray[i] = array1.at(i);
        resArray[i+array1.getSize()] = array2.at(i);
    }

    return resArray;
}

ostream& operator<<(ostream &stream, const IntArray &array) {
    for (int i = 0; i < array.getSize(); ++i) {
        stream << "a[" << i << "]" << array.at(i) << '\n' << endl;
    }

    return stream;
}
