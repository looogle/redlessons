#include <cstddef>
#include <cstdint>

#include "crc8.h"

using namespace std;

Crc8::Crc8(const uint8_t* pData, const size_t dataLength) :
    Crc(pData, dataLength)
{ }

Crc8::~Crc8() { }

void Crc8::calcCrc()
{
    uint8_t crc = 0xFF;
    uint32_t j;

    for (size_t i = 0; i < m_dataLength; ++i) {
        crc ^= m_pData[i];

        for (j = 0; j < 8; ++j)
            crc = crc & 0x80 ? (crc << 1) ^ 0x31 : crc << 1;
    }

    m_crc = crc;
}