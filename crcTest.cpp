#include <iostream>
#include <cstddef>
#include <cstdint>

#include "crc.h"
#include "crcFactory.h"

using namespace std;

void emptyLine();

int main() {
    uint8_t data[5] = {0, 1, 2, 3, 4};
    const size_t dataLength = sizeof(data);

    Crc* crc1 = CrcFactory::makeCrcFromData(data, dataLength);  // make and
    crc1->calcCrc();                                            // calc CRC8

    Crc* crc11 = CrcFactory::makeCrcFromData(data, dataLength); // another make
    crc11->calcCrc();                                           // and calc CRC8

    data[0] = 1; // say, that CRC16 needed
    Crc* crc2 = CrcFactory::makeCrcFromData(data, dataLength);  // make and
    crc2->calcCrc();                                            // calc CRC16

    cout << "crc1 is CRC8: " << crc1->getCrc() << endl;
    cout << "crc11 is CRC8: " << crc11->getCrc() << endl;
    cout << "crc1 is CRC16: " << crc2->getCrc() << endl;

    emptyLine();
    cout << "Test overloaded operator=() for base class:" << endl;
    cout << " crc1 == crc2 = " << (*crc1 == *crc2) << endl;
    cout << " crc1 == crc11 = " << (*crc1 == *crc11) << endl;

    return 0;
}

inline void emptyLine() {
    cout << endl;   // just go to next line
}